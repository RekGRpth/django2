#!/bin/sh -ex

#docker build --tag rekgrpth/django2 .
#docker push rekgrpth/django2
docker pull rekgrpth/django2
docker volume create django2
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker stop django2 || echo $?
docker rm django2 || echo $?
docker run \
    --detach \
    --env ALLOWED_HOSTS="django2.$(hostname -f) django2-$(hostname -f)" \
    --env DEBUG=1 \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname django2 \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=bind,source=/run/postgresql,destination=/run/postgresql \
    --mount type=bind,source=/run/uwsgi,destination=/run/uwsgi \
    --mount type=volume,source=django2,destination=/home \
    --name django2 \
    --network name=docker \
    --restart always \
    rekgrpth/django2 uwsgi --ini django2.ini
