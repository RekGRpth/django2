FROM rekgrpth/gost
ENV GROUP=django \
    PYTHONIOENCODING=UTF-8 \
    PYTHONPATH=/usr/local/lib/python3.8:/usr/local/lib/python3.8/lib-dynload:/usr/local/lib/python3.8/site-packages:/usr/lib/python38.zip:/usr/lib/python3.8:/usr/lib/python3.8/lib-dynload:/usr/lib/python3.8/site-packages \
    PYTHONUTF8=1 \
    USER=django
VOLUME "${HOME}"
ADD django_webix_admin_utils.diff /tmp/
ADD options.diff /tmp/
RUN exec 2>&1 \
    && set -ex \
    && addgroup -S "${GROUP}" \
    && adduser -D -S -h "${HOME}" -s /sbin/nologin -G "${GROUP}" "${USER}" \
    && ln -s pip3 /usr/bin/pip \
    && ln -s pydoc3 /usr/bin/pydoc \
    && ln -s python3 /usr/bin/python \
    && ln -s python3-config /usr/bin/python-config \
    && apk add --no-cache --virtual .build-deps \
        g++ \
        gcc \
        gdal-dev \
        jpeg-dev \
        linux-headers \
        make \
        musl-dev \
        openldap-dev \
        patch \
        pcre2-dev \
        pcre-dev \
        postgresql-dev \
        py3-pip \
        python3-dev \
        zlib-dev \
    && pip install --no-cache-dir --ignore-installed --prefix /usr/local \
        django \
        django-bootstrap4 \
        django-filter \
        django-tables2 \
        django-tablib \
        django-webix \
        feincms \
        gdal \
        ipython \
        psycopg2 \
        uwsgi \
    && patch /usr/local/lib/python3.8/site-packages/django_webix/admin_webix/options.py /tmp/options.diff \
    && patch /usr/local/lib/python3.8/site-packages/django_webix/admin_webix/templatetags/django_webix_admin_utils.py /tmp/django_webix_admin_utils.diff \
    && apk add --no-cache --virtual .django-rundeps \
        python3 \
        $(scanelf --needed --nobanner --format '%n#p' --recursive /usr/local | tr ',' '\n' | sort -u | grep -v 'libmustach.so' | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }') \
    && (strip /usr/local/bin/* /usr/local/lib/*.so || true) \
    && apk del --no-cache .build-deps \
    && rm -rf /usr/src /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man /tmp/* \
    && echo done
